package ru.t1.zkovalenko.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractUserOwnerModel {

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;

}
