package ru.t1.zkovalenko.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.Task;

@NoArgsConstructor
public class TaskShowByIdResponse extends AbstractTaskResponse {

    public TaskShowByIdResponse(@Nullable final Task task) {
        super(task);
    }

}
