package ru.t1.zkovalenko.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class UserLoginResponse extends AbstractResultResponse {

    @Nullable
    private String token;

    public UserLoginResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

    public UserLoginResponse(@Nullable String token) {
        this.token = token;
    }

}
